package com.hanggold.mybatis.test;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.parsing.XPathParser;
import org.junit.Test;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.List;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-16 08:54
 * @since 1.0.0
 */
public class TestXmlParser {

    @Test
    public void parseTest() {

        InputStream inputStream =
                XmlDemoTest.class.getClassLoader().getResourceAsStream("mapper/TestMapper.xml");

        XPathParser pathParser = new XPathParser(inputStream);


        XNode xNode = pathParser.evalNode("/mapper");


        XNode select = xNode.evalNode("select");

//        List<XNode> xnodes  = xNode.evalNodes("select");
//
//        for (XNode context : xnodes){
//
//            System.out.println("Context: "+ context);
//
//            List<XNode> c =  context.getChildren();
//
//        }


        NodeList children = select.getNode().getChildNodes();

        for (int i = 0; i < children.getLength(); i++) {

            Node node = children.item(i);

            System.out.println("node: " + node.toString() + " type: " + node.getNodeType());

        }


//        List<XNode> chdildren = select.getChildren();
//
//        System.out.println("children size "+ chdildren.size());
//
//        System.out.printf("%s \n", select);
    }

    @Test
    public void incTest() {
        InputStream inputStream =
                XmlDemoTest.class.getClassLoader().getResourceAsStream("mapper/TestMapper.xml");

        XPathParser pathParser = new XPathParser(inputStream);


        XNode xNode = pathParser.evalNode("/mapper");


        XNode select = xNode.evalNode("select");


        Node source = select.getNode();
//        if (source.getNodeName().equals("include")) {
//        }

        dgSql(source);

    }


    private void dgSql(Node source) {

        System.out.println("nodeName: "+ source.getNodeName()+" type: "+ source.getNodeType());

        NodeList children = source.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child =  children.item(i);
            if (child.getNodeType() == Node.TEXT_NODE || child.getNodeType() == Node.CDATA_SECTION_NODE){
                System.out.println(child.getNodeName()+" , "+ child.getNodeValue());
            }
        }
    }

}
