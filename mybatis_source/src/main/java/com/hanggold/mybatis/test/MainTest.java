package com.hanggold.mybatis.test;

import com.hanggold.mybatis.mapper.TestMapper;
import org.apache.ibatis.scripting.xmltags.DynamicContext;
import org.apache.ibatis.session.Configuration;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

import static java.lang.Character.MIN_HIGH_SURROGATE;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-02 17:48
 * @since
 **/
public class MainTest {


    static enum PR {

        One, Two

    }


    @Test
    public void enumTest() {

        System.out.printf("one: %s \n", PR.One.toString());

        PR pr = PR.valueOf(PR.One.toString());

        System.out.println(pr);
    }


    @Test
    public void simplatTest() {

        String simpleName = TestMapper.class.getSimpleName();

        System.out.println("sim: " + simpleName);
    }


    @Test
    public void mapTest() {

        Map<String, String> map = new HashMap<>(2);

        map.putIfAbsent("name", "xxxx");

        map.putIfAbsent("name", "44444");


        String targetValue = map.computeIfAbsent("name", k -> {
            return "new value";
        });


        System.out.printf("newValye: %s \n", targetValue);


        // 替换值
        map.computeIfPresent("name", (k, v) -> {
            System.out.printf("source,   k: %s, v:%s \n", k, v);
            return "helloword";
        });


        map.forEach((k, v) -> {
            System.out.printf("k: %s, v:%s \n", k, v);
        });


    }


    @Test
    public void arrayTest() {

        String str = "select * from  ${name} ${ssss}";
        char[] src = str.toCharArray();
        int startIndex = str.indexOf("${");
        int o = str.indexOf("${", startIndex + 2);
        System.out.println("o: " + o);
        System.out.println(src[startIndex - 1]);
        System.out.println("startIndex: " + startIndex);
        System.out.println(str.substring(startIndex + 1));


        for (char c : src) {

//            System.out.printf("字符 %s \n",c);
        }

        //${

        System.out.println();

        final StringBuilder builder = new StringBuilder();

        builder.append(src, 0, startIndex);

        System.out.println(builder.toString());


    }


    @Test
    public void characterTest() {
        char a = MIN_HIGH_SURROGATE;

        System.out.println(a);

        char low = 'Z';

        System.out.println((int) low);

        System.out.println((int) 'z');
    }


    @Test
    public void dynamicContextTest() {
        DynamicContext dynamicContext = new DynamicContext(new Configuration(), 2);

        System.out.println(dynamicContext);


    }

    @Test
    public void stringJoinTest() {

        StringJoiner stringJoiner = new StringJoiner(",", "[", "]");

        stringJoiner.add("hello").add("world").add("china");

        System.out.println(stringJoiner.toString());

    }


    @Test
    public void parseTest() {

        String sql = "select * from t_user where name  = \\${name} and id = ${id}";

        char[] src = sql.toCharArray();

        int startIndex = sql.indexOf("${");

        System.out.println("startIndex: " + startIndex);
        StringBuilder sb = new StringBuilder();

        sb.append(src, 0, startIndex);

        System.out.println(sb);


        int endIndex = sql.indexOf("}");

        System.out.println("endIndex " + endIndex);

        sb.append(src, startIndex + 2, endIndex - startIndex - 2);


//
        System.out.println(sb);


    }


    @Test
    public void wasTest() {
        String expression = "name,jdbcType=xxxx";
        int p = 0;
        for (int i = p; i < expression.length(); i++) {
            if (expression.charAt(i) > 0x20) {
                System.out.println(i);
            }
        }
    }


    @Test
    public void deTest() {
        String expression = "name,jdbcType=xxxx";
        int a = 0;
        String ss = ",:";
        for (int i = 0; i < expression.length(); i++) {
            if (ss.indexOf(expression.charAt(i)) > -1) {
                a = i;
                break;
            }
        }

        System.out.println(expression.substring(0, a));
        System.out.println("a: " + a);
        String newContent = expression.substring(a);
        System.out.println("newContent: " + newContent);

    }

    @Test
    public void assciTest() {

        String str = "abcABC123";

        char[] src = str.toCharArray();

        int a = 0x20;

        char cc = (char) a;

        System.out.println("cc " + cc);

        System.out.println(Integer.toHexString(a));

        for (char c : src) {
            System.out.println(Integer.toHexString((int) c));
        }

    }

    @Test
    public void arrTest() {

        Map<String, String> map = new HashMap<String, String>() {{
            put("name", "xx");
            put("age", "12");
        }};


        String[] arr = map.keySet().toArray(new String[0]);


        Arrays.asList(arr).forEach(System.out::println);


    }


    @Test
    public void newMapTest() {

        Map<String, String> map = new HashMap<>();

        map.put("key","xxxx123");

        String value = map.computeIfAbsent("key", (b) -> {
            return "xx";
        });

        System.out.println("va: " + value);


    }



}
