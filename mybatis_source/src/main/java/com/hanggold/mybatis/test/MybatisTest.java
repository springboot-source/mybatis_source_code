package com.hanggold.mybatis.test;

import com.hanggold.mybatis.bean.UserInfo;
import com.hanggold.mybatis.ref.RefUser;
import org.apache.ibatis.reflection.Reflector;
import org.junit.Test;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-09-06 15:15
 * @since 1.0.0
 **/
public class MybatisTest {

    @Test
    public void refcTest(){

        Reflector reflector = new Reflector(RefUser.class);

        boolean b = reflector.hasSetter("xx");

        System.out.println(b);
    }

}
