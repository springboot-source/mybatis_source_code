package com.hanggold.mybatis.test;

import com.hanggold.mybatis.mapper.TestMapper;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-20 08:23
 * @since 1.0.0
 **/
public class RefTest {


    @Test
    public void methonTest() throws NoSuchMethodException {


        Class<?>[] classes = TestMapper.class.getDeclaredClasses();

        Method method = TestMapper.class.getMethod("count");

        Class m = method.getDeclaringClass();


       Type rt =  method.getGenericReturnType();

//        Class rtc = method.getReturnType();


        System.out.println(rt instanceof TypeVariable);

        System.out.println(rt instanceof GenericArrayType);

        System.out.println(rt instanceof ParameterizedType);

        System.out.println(rt instanceof Class<?>);

       boolean voidType =  void.class.equals(rt);

        System.out.println("voidType: "+ voidType);

        final Annotation[][] paramAnnotations = method.getParameterAnnotations();

        final Class<?>[] paramTypes = method.getParameterTypes();
        System.out.println(1);
    }



    @Test
    public void methonOneTest() throws NoSuchMethodException {


        Class<?>[] classes = TestMapper.class.getDeclaredClasses();

        Method method = TestMapper.class.getMethod("count");

        System.out.println(method.getGenericReturnType());

        System.out.println(method.getDeclaringClass());

        System.out.println(method.getReturnType());

        System.out.println(method.getReturnType().equals(void.class));

    }

    @Test
    public void methonTwoTest() throws NoSuchMethodException {

        Class<?>[] classes = TestMapper.class.getDeclaredClasses();

        Method method = TestMapper.class.getMethod("count", new Class[]{int.class,int.class});

        List<String> names = Stream.of(method.getParameters()).map(e->e.getName()).collect(Collectors.toList());
        Stream.of(method.getParameters()).forEach(e->{
            System.out.println(e.getName());
        });


        String name = names.get(4);

//        System.out.println("name: "+ name);
    }

}
