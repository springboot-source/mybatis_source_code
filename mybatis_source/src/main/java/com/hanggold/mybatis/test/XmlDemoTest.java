package com.hanggold.mybatis.test;

import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

public class XmlDemoTest {


    @Test
    public void oneXmlTest() throws ParserConfigurationException, IOException, SAXException {

        InputStream inputStream = XmlDemoTest.class.getClassLoader().getResourceAsStream("mybatis.test.xml");

        InputSource inputSource = new InputSource(inputStream);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = factory.newDocumentBuilder();

        Document document = documentBuilder.parse(inputSource);



    }


}
