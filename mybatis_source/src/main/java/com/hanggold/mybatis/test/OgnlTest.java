package com.hanggold.mybatis.test;


import ognl.OgnlContext;
import ognl.OgnlException;
import ognl.PropertyAccessor;

import java.util.Map;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-28 11:02
 * @since 1.0.0
 **/
public class OgnlTest {






    static class ContextAccessor implements PropertyAccessor{

        @Override
        public Object getProperty(Map context, Object target, Object name) throws OgnlException {
            return null;
        }

        @Override
        public void setProperty(Map context, Object target, Object name, Object value) throws OgnlException {

        }

        @Override
        public String getSourceAccessor(OgnlContext context, Object target, Object index) {
            return null;
        }

        @Override
        public String getSourceSetter(OgnlContext context, Object target, Object index) {
            return null;
        }
    }
}






