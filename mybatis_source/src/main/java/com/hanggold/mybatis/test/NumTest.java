package com.hanggold.mybatis.test;

import org.junit.Test;

import java.util.Random;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-13 08:19
 * @since
 **/
public class NumTest {

    @Test
    public void one(){
        Random rnd = new Random();
        boolean toBe = rnd.nextBoolean();
        Number result = (toBe || !toBe) ?
                new Integer(3) : new Float(1);
        System.out.println(result);
    }


    @Test
    public void numberCon(){
        long a = 1234L;

        int b  = (int)a;
        System.out.println(b);
    }


    @Test
    public void doubleTest(){

        Double s = 100.00;

        System.out.println(s == 100);

    }
}
