package com.hanggold.mybatis.url;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-02 14:21
 * @since
 **/
public class FileReadTest {

    public static void main(String[] args) throws IOException {

        URL url = new URL("file:///xxx.txt");

        InputStream inputStream = url.openConnection().getInputStream();

         InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        BufferedReader br = new BufferedReader(inputStreamReader);

        System.out.println(br.readLine());



    }

}
