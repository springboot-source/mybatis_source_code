package com.hanggold.mybatis.dg;

import com.alibaba.fastjson.JSON;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-16 17:13
 * @since 1.0.0
 **/
public class GdData {

    public static void main(String[] args) {

//        System.out.println(new GdData().sum(10));

        U u = new U("1", "0", "第一级");

        U u2 = new U("2", "0", "第一级");

        U u3 = new U("11", "1", "第一级");

        U u4 = new U("22", "2", "第一级");

        List<U> sources = Arrays.asList(u, u2, u3, u4);

        List<U> targets = new GdData().dgTree("0", sources);

      String result =   JSON.toJSONString(targets);

        System.out.println("result: "+ result);
    }

    public int sum(int count) {
        if (count <= 0) {
            return 0;
        } else {
            return count + sum(count - 1);
        }

    }


    public List<U> dgTree(String parent, List<U> sources) {
        List<U> targets = new ArrayList<>();
        for (U u : sources) {
            if (u.getParentId().equals(parent)) {
                targets.add(u);
                return targets;
            } else {
                u.setChildren(dgTree(u.id, sources));
                targets.add(u);
            }
        }
       return null;
    }


    static class U {


        private String id;

        private String parentId;

        private String name;

        private List<U> children;

        public U(String id, String parentId, String name) {
            this.id = id;

            this.parentId = parentId;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<U> getChildren() {
            return children;
        }

        public void setChildren(List<U> children) {
            this.children = children;
        }
    }
}
