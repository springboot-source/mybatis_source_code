package com.hanggold.mybatis.proxy;

import java.lang.reflect.Proxy;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-02 09:01
 * @since
 **/
public class MainDemo {



    public static void main(String[] args) {

        IProxy proxy = (IProxy) Proxy.newProxyInstance(IProxy.class.getClassLoader(),ActedProxy.class.getInterfaces(),
                new MainProxy(new ActedProxy()));

        proxy.say();


    }

}
