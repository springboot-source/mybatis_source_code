package com.hanggold.mybatis.proxy;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-02 09:00
 * @since
 **/
public interface IProxy {

    public void say();
}
