package com.hanggold.mybatis.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-02 08:59
 * @since
 **/
public class MainProxy implements InvocationHandler {

    private Object target;



    public MainProxy(Object target){
        this.target = target;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println("代理的对象: "+ proxy.getClass().getName());
//
        System.out.println("代理的method: "+ method);
//
        System.out.println("代理传输的args: "+ args);

        method.invoke(target,args);

//        method.invoke()


        return null;
    }
}
