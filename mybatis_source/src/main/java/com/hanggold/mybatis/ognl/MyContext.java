package com.hanggold.mybatis.ognl;

import ognl.*;
import org.apache.ibatis.scripting.xmltags.OgnlClassResolver;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-09-02 10:59
 * @since 1.0.0
 **/
public class MyContext {

    private static final DefaultMemberAccess MEMBER_ACCESS = new DefaultMemberAccess(true);

    private static final DefaultClassResolver CLASS_RESOLVER = new DefaultClassResolver();

    static {
        OgnlRuntime.setPropertyAccessor(ContextMap.class, new ContextAccessor());
    }

    public static void main(String[] args) throws OgnlException {

        User user = new User("ognl", 12);

        ContextMap contextMap = new ContextMap();

        contextMap.put("username", "监听数据");

        Map<String,Object> address = new HashMap<>(2);

        address.put("area","苏州");

        contextMap.put("address",address);

        contextMap.put("user",user);
        Map context = Ognl.createDefaultContext(contextMap, MEMBER_ACCESS, CLASS_RESOLVER, null);


//        Object username = Ognl.getValue("username", context,contextMap);

//        System.out.println("username: " + username);


        Object age = Ognl.getValue("user.age",context,contextMap);

        System.out.println("age: "+ age);

    }

    static class ContextMap extends HashMap<String, Object> {

        @Override
        public Object get(Object key) {
            System.out.printf("开始 获取token %s \n",key);
//            System.out.println("xx " + super.containsKey(key));
            return super.get(key);
        }
    }

    static class ContextAccessor implements PropertyAccessor {

        @Override
        public Object getProperty(Map context, Object target, Object name) throws OgnlException {

            System.out.printf("getproperty target:%s, context:%s ,name:%s \n", target, context, name);
            Map map = (Map) target;
            Object result = map.get(name);
            System.out.printf("result : %s \n", result);
            return result;
        }

        @Override
        public void setProperty(Map context, Object target, Object name, Object value) throws OgnlException {

        }

        @Override
        public String getSourceAccessor(OgnlContext context, Object target, Object index) {
            return null;
        }

        @Override
        public String getSourceSetter(OgnlContext context, Object target, Object index) {
            return null;
        }
    }

    static class User {

        private String username;

        private int age;

        public User(String username, int age) {
            this.username = username;
            this.age = age;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }
}
