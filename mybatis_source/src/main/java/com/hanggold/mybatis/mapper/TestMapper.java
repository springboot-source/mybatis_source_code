package com.hanggold.mybatis.mapper;

import com.hanggold.mybatis.bean.UserInfo;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-02 15:16
 * @since 1.0.0
 **/
public interface TestMapper {

    public int count();

    public int count(int a, int b);

    public int count(UserInfo userInfo);

}
