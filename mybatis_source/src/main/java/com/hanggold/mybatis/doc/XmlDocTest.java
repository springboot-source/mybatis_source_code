package com.hanggold.mybatis.doc;

import com.hanggold.mybatis.MainApplication;
import org.apache.ibatis.builder.xml.XMLConfigBuilder;
import org.apache.ibatis.parsing.XNode;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.InputStream;

import static org.w3c.dom.Node.ELEMENT_NODE;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-01 16:23
 * @since
 **/
public class XmlDocTest {


    @Test
    public void initXml() throws Exception {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        factory.setNamespaceAware(false);

        factory.setIgnoringComments(true);

        factory.setIgnoringElementContentWhitespace(false);

        factory.setCoalescing(false);

        factory.setExpandEntityReferences(true);

        DocumentBuilder builder = factory.newDocumentBuilder();

        InputStream inputStream = MainApplication.class.getClassLoader().getResourceAsStream("mybatis-config.xml");

        Document document = builder.parse(inputStream);

        XPathFactory xPathFactory = XPathFactory.newInstance();

        XPath xPath = xPathFactory.newXPath();

        Node node = (Node)xPath.evaluate("/configuration",document, XPathConstants.NODE);

//        node.getBaseURI()

        System.out.printf("baseurl:  %s \n",node.getBaseURI());
        NodeList nodeList =  node.getChildNodes();

        for (int i = 0 ; i < nodeList.getLength(); i++) {
            Node node1 = nodeList.item(i);
            System.out.printf("子元素: %s \n",node1.getNodeName());
//            if (node.getNodeType() == Node.ELEMENT_NODE) {
//                System.out.printf("子元素: %s \n",node.getTextContent());
//            }
        }
        node.getFirstChild();


        System.out.println("obj: "+ node.getClass().getName());


    }



    @Test
    public void readXmlTest() throws Exception {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        factory.setNamespaceAware(false);

        factory.setIgnoringComments(true);

        factory.setIgnoringElementContentWhitespace(false);

        factory.setCoalescing(false);

        factory.setExpandEntityReferences(true);

        DocumentBuilder builder = factory.newDocumentBuilder();

        InputStream inputStream = MainApplication.class.getClassLoader().getResourceAsStream("mapper/TestMapper.xml");

        Document document = builder.parse(inputStream);

        XPathFactory xPathFactory = XPathFactory.newInstance();

        XPath xPath = xPathFactory.newXPath();

        Node node = (Node)xPath.evaluate("/mapper/select",document, XPathConstants.NODE);

        System.out.printf("testMapper children size  %d \n",node.getChildNodes().getLength());

        for (int i= 0; i<node.getChildNodes().getLength();i++){
            Node child = node.getChildNodes().item(i);
            // 表示是元素
            if (child.getNodeType() == ELEMENT_NODE){
                NamedNodeMap namedNodeMap =  child.getAttributes();

                for (int attrIndex=0;attrIndex<namedNodeMap.getLength();attrIndex++){
                    Node attr =  namedNodeMap.item(attrIndex);
                    System.out.printf("attrname  %s attrvalue %s \n",attr.getNodeName(),attr.getNodeValue());
                }


            }
//            System.out.printf("child type  %d , node name %s \n",child.getNodeType(),child.getNodeName());


        }


    }


}
