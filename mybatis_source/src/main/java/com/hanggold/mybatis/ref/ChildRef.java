package com.hanggold.mybatis.ref;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-09-05 15:56
 * @since 1.0.0
 **/
public class ChildRef {

    private Integer age;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
