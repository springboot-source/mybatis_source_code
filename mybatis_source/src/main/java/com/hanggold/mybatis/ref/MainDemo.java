package com.hanggold.mybatis.ref;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-09-05 15:53
 * @since 1.0.0
 **/
public class MainDemo {

    public static void main(String[] args) {

        Method[] methods = RefUser.class.getDeclaredMethods();

        try {

            BeanInfo beanInfo =  Introspector.getBeanInfo(RefUser.class);

            PropertyDescriptor[] propertyDescriptors =  beanInfo.getPropertyDescriptors();

            System.out.println(1);

        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        System.out.println(1);

    }
}
