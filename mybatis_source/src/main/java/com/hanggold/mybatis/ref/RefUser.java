package com.hanggold.mybatis.ref;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-09-05 15:53
 * @since 1.0.0
 **/
public class RefUser {

    private String username;

    private ChildRef childRef;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ChildRef getChildRef() {
        return childRef;
    }

    public void setChildRef(ChildRef childRef) {
        this.childRef = childRef;
    }
}
