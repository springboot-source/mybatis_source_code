package com.hanggold.mybatis.bean;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-08-29 15:05
 * @since 1.0.0
 **/
public class UserInfo {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
