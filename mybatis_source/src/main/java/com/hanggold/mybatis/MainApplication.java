package com.hanggold.mybatis;

import com.hanggold.mybatis.bean.UserInfo;
import com.hanggold.mybatis.mapper.TestMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

public class MainApplication {

    public static void main(String[] args) {

        InputStream inputStream = MainApplication.class.getClassLoader().getResourceAsStream("mybatis-config.xml");

        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();

        TestMapper testMapper = sqlSession.getMapper(TestMapper.class);

        System.out.printf("table t_test count %d \n",testMapper.count());

    }


}
